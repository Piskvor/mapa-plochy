var area = 17400000 //m2

function thousandSep(number) { // Elias Zamaria, https://stackoverflow.com/a/2901298
  return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
};

var map = new L.map('map', {
    inertia:false
}).setView([50.0835494, 14.4341414], 11);

 map.scrollWheelZoom.disable();

var basemap = L.tileLayer(
	'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
	{
		attribution: 'mapa &copy; <a href="http://osm.org/copyright">OpenStreetMap</a>'
	}
).addTo(map);
var bounds, rect;

function drawRect(area) {
    var side = Math.sqrt(area);

    var center = map.getCenter();
    var ccle = L.circle([center.lat, center.lng], {radius: side / 2}).addTo(map); // getBounds() is dependent on projection, therefore this hack 
    bounds = ccle.getBounds();
    ccle.removeFrom(map);

    rect = L.rectangle(bounds, {
        color: '#045a8d', 
        weight: 2,
        fillOpacity: 0.4
    }).addTo(map);

    rect.bindTooltip(thousandSep(area / 10000) + ' ha', {permanent: true, direction:'center'}).openTooltip();
};

drawRect(area);
map.fitBounds(bounds);

map.on('dragend', function () {
    map.removeLayer(rect);
    drawRect(area)
});